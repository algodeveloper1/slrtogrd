
"""
Created on Mon Apr 16 09:37:03 2018

@author: plousser
"""

from osgeo import gdal
from gdalconst import GA_ReadOnly
import numpy as np

import os, os.path, optparse, sys

def SlrToGrdProj(slrFile,azfile,rgfile,outputfile):
    'Projection of an image from Slant Range geometry to Ground Projected geometry'
    print('Open the file')
    # Open input image in slant range geometry:
    if os.path.exists(slrFile):
            # Open original image in slant range geometry:
            slr_image_driver = gdal.Open(slrFile, 0)
            slr_image = slr_image_driver.ReadAsArray(0,0,1,1)
            print(slr_image.shape)
    else : 
        print('The input file does not exist in the directory')

    
    """get the campaign and scene """
    #slrFilename=slrFile.split('/')[len(inputfile.split('/'))-1]
    #campaign, scene,reste = slrFilename.split('_',2)
    # Open Azimuth coordinates file:
    #azimuthFile=campaign+'_'+scene+'_az.tiff'
    if os.path.exists(azfile):
        Azimuth_driver = gdal.Open(azfile,GA_ReadOnly)
        Azimuth = Azimuth_driver.ReadAsArray()
        print(Azimuth.shape)
    else : 
        print('No azimuth file in the directory')
    
    # Open Range coordinates file:
    #rangeFile=campaign+'_'+scene+'_rg.tiff'
    if os.path.exists(rgfile):
        Range_driver = gdal.Open(rgfile, GA_ReadOnly)
        Range = Range_driver.ReadAsArray()
        print(Range.shape)
    else : 
        print('No range file in the directory')
    
    # Mask of the data inside the GRD projected image:
    mask = np.logical_and(Range!=55537, Azimuth!=55537)
    print(mask)
    # Create an empty image of NaN:
    grd_image = np.full((Range_driver.RasterYSize, Range_driver.RasterXSize), np.NaN, dtype=slr_image.dtype)
    print(grd_image)
    # Create the image in the ground projected geometry:
    
    outdriver = gdal.GetDriverByName('GTiff')
    grd_image_driver = outdriver.Create(outputfile, Range_driver.RasterXSize, Range_driver.RasterYSize, slr_image_driver.RasterCount, slr_image_driver.GetRasterBand(1).DataType)
    grd_image_driver.SetGeoTransform(Range_driver.GetGeoTransform())
    grd_image_driver.SetProjection(Range_driver.GetProjection())
    
    for band in range(slr_image_driver.RasterCount):
        # Read original image in slant range geometry:
        slr_image = slr_image_driver.GetRasterBand(band+1).ReadAsArray()
        
        # Project the image in the ground projected geometry:
        
        grd_image[mask] = slr_image[Azimuth[mask], Range[mask]]
        
        # Save the corresponding band of the image in the ground projected geometry:
        grd_image_driver.GetRasterBand(band+1).WriteArray(grd_image)
    
    # Close data sets:
    slr_image_driver = None
    Azimuth_driver = None
    Range_driver = None
    grd_image_driver = None

 
if __name__ == '__main__':
    import quicklook_raster 
    from properties.p import Property
    if os.path.isfile('/projects/slrtogrd/conf/configuration.properties'):
            
        configfile='/projects/slrtogrd/conf/configuration.properties'
        print('The processing is launched using ', configfile)
        prop=Property()
        prop_dict=prop.load_property_files(configfile)
    
        SlrToGrdProj(prop_dict['inputfile'], prop_dict['azfile'],prop_dict['rgfile'],prop_dict['outputfile'])
        quicklook_raster.main(prop_dict['outputfile'])